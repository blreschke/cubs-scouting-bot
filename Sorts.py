import discord
import os
from PlayerData import *
from SiteAccess import *


"""
Parameters and return type are the same for every sort, unless they are not async, in which they only have data

    Parameters:
        ctx - client(?) to send error codes to bot with
        data - a list of dictionaries with each element containing releveant PA data
        param - parameter of command being passed into sort, containing the command and arguments
            i.e. "Outs 1,2", "game starts", "following steals"
    Return:
        If an error is sent - False, else...
        a Newly sorted list from data parameter
"""

async def byOuts(ctx, data, param):
    """
    Sorts by specified out(s) in param
    """
    if len(param.strip()) <= 4:
        await ctx.send("\"OUTS\" found no code to sort by.")
        return False
    parameters = param[4:].split(",")
    try:
       parameters = [int(i.strip()) for i in parameters]
    except:
       await ctx.send("Invalid input in \"OUTS\".")
       return False
    return sortByParameter(data, ["outs"], parameters)

async def byOBC(ctx, data, param):
    """
    Sorts by specified OBC in param
    """
    if len(param.strip()) <= 3:
        await ctx.send("\"OBC\" found no code to sort by.")
        return False
    obcs = param[3:].split(",")
    obcs = [int(i.strip()) for i in obcs]
    return sortByParameter(data, ["obc"], obcs)

async def byInnings(ctx, data, param):
    """
    Sorts data based on inning, can input multiple innings in a comma list
    """
    if len(param.strip()) <= 6:
        await ctx.send("\"Inning\" found no inning(s) to sort by.")
        return False
    inning_nums = param[6:].strip().split(",")
    innings = []
    for i in inning_nums:
        i = i.strip()
        innings.append("B" + i)
        innings.append("T" + i)
    return sortByParameter(data, ["inning"], innings)
            
async def byCurrent(ctx, data, param):
    """
    Sorts the data based on the current game, either MiLR or MLR based on param
    """
    if len(param.strip()) <= 7:
        await ctx.send("Select a league for \"current\" parameter (MLR or MiLR)")
        return False
    league = param[7:].strip()
    if league.lower() == "milr":
        active_game = activeTeamGame("HEL", True)
        if active_game == False:
            await ctx.send("\"" + league + "\" does not have an active game.")
            return False
        else:
            data = sortByParameter(data, ["game", "id"], [active_game["id"]])
    elif league.lower() == "mlr":
        active_game = activeTeamGame("CHC", False)
        if active_game == False:
            await ctx.send("\"" + league + "\" does not have an active game.")
            return False
        else:
            data = sortByParameter(data, ["game", "id"], [active_game["id"]])
    elif league.lower() == "lemons":
        active_game = activeTeamGame("LMN", True)
        if active_game == False:
            await ctx.send("\"" + league + "\" does not have an active game.")
            return False
        else:
            data = sortByParameter(data, ["game", "id"], [active_game["id"]])
    elif league.lower() == "limes":
        active_game = activeTeamGame("LIM", True)
        if active_game == False:
            await ctx.send("\"" + league + "\" does not have an active game.")
            return False
        else:
            data = sortByParameter(data, ["game", "id"], [active_game["id"]])
    else:
        await ctx.send("\"" + league + "\" is not a valid league.")
        return False
    if data == []:
        await ctx.send("This player has no PAs in the current game.")
        return False
    return data

async def byPitchRange(ctx, data, param, isPitcher):
    """
    Sorts data by pitch range in parameter, range inputted in the format "for x00s"
    """
    if (isPitcher):
        dict_path = "pitch"
    else:
        dict_path = "swing"

    if len(param.strip()) <= 4:
        await ctx.send("Select pitch range to sort by.")
        return False

    try:
        pitch_range = param[4:].strip()
        pitch_range = int(pitch_range[:-1])
    except:
        await ctx.send("Invalid range for \"For\" argument.")
        return False
    if pitch_range % 100 == 0:
        return sortByRange(data, [dict_path], pitch_range, pitch_range + 99)
    else:
        await ctx.send("\"For\" range must be divisible by 100.")
        return False

async def bySeason(ctx, data, param):
    if len(param.strip()) <= 7:
        await ctx.send("Select season(s) to sort by. ")
        return False
    seasons = param[7:].strip().split(",")
    seasons = [i.strip().lower() for i in seasons]
    sort_params = [] 

    for i in seasons:
        if i.endswith("i"):
            try:
                sort_params.append(int(i[0]) - 100)
            except:
                await ctx.send("\"" + i + "\" is an invalid season input.")
                return False
        else:
            try:
                sort_params.append(int(i))
            except:
                await ctx.send("\"" + i + "\" is an invalid season input.")
                return False
    return sortByParameter(data, ["season"], sort_params)

async def byLeague(ctx, data, param):
    if len(param.strip()) <= 6:
        await ctx.send("Select a league to sort by")
        return False
    league = param[6:].strip().lower()
    if (league != "milr" and league != "mlr"):
        await ctx.send("\"" + league + "\" is not a valid league.")
        return False
    return sortByParameter(data, ["league"], league)

async def byFollowing(ctx, data, param):
    if len(param.strip()) <= 9:
        await ctx.send("\"Following\" found no result found to sort by.")
        return False
    param = param[9:].strip()

    #cut game starts
    data = sortByParameter(data, ["gameStart"], [False])

    byIS = await byFollowingInningStart(ctx, data, param)
    if byIS != "Invalid":
        return byIS

    byGS = await byFollowingGameStart(ctx, data, param)
    if byGS != "Invalid":
        return byGS

    byResults = byFollowingResults(data, param)
    if byResults != "Invalid":
        return byResults

    byDiffRange = await byFollowingDiffs(ctx, sortByParameter(data, ["previousGameStart"], [False]), param)
    if byDiffRange != "Invalid":
        return byDiffRange

    byRange = await byFollowingRange(ctx, data, param)
    if byRange != "Invalid":
        return byRange

    byCustomRange = await byFollowingCustomRange(ctx, data, param)
    if byCustomRange != "Invalid":
        return byCustomRange

    else:
        try:
            nums = param.split(",")
            nums = [int(i) for i in nums] 
        except:
            await ctx.send("\"Following\" has invalid arguement.")
            return False
        return sortByParameter(data, ["previousPitch"], nums)

def byFollowingResults(data, param):
    all_results = ["bb", "1b", "2b", "hr", "3b", "bunt 1b", "ibb", "auto bb", "k", "po", "fo", "rgo", "lgo", "auto k", "bunt sac", "bunt k", "cs 2b", "cs 3b", "cs home", "cms home", "cms 3b", "steal 2b", "steal 3b", "steal home"]

    if param.strip().lower() == "out" or param.strip().lower() == "outs":
        results = ["k", "po", "fo", "rgo", "lgo", "auto k", "bunt sac", "bunt k", "cs 2b", "cs 3b", "cs home", "cms home", "cms 3b"]
        return sortByParameter(data, ["previousResult"], results)

    elif param.strip().lower() == "hit" or param.strip().lower() == "hits":
        results = ["bb", "1b", "2b", "hr", "3b", "bunt 1b", "ibb", "auto bb"]
        return sortByParameter(data, ["previousResult"], results)

    elif param.strip().lower() == "steal" or param.strip().lower() == "steals":
        results = ["steal 2b", "cs 2b", "steal 3b", "cs 3b", "sb home", "steal home", "cs home", "cms 3b", "cms home" "sb"]
        return sortByParameter(data, ["previousResult"], results)
    else:
        results = param.split(",")
        results = [i.strip() for i in results]
        is_result = all(item in all_results for item in results)
        if is_result:
            return sortByParameter(data, ["previousResult"], results)
        else:
            return "Invalid"

async def byFollowingRange(ctx, data, param):
    if param.endswith("s"):
        try:
            range_num = int(param[:-1])
        except:
            await ctx.send("Invalid argument in \"Following\".")
            return False
        if range_num % 100 == 0:
            return sortByRange(data, ["previousPitch"], range_num, range_num + 99)
        else:
            await ctx.send("\"Following\" range must be divisible by 100.")
            return False
    return "Invalid"

async def byFollowingDiffs(ctx, data, param):
    if param.endswith("s") and (param.startswith("+") or param.startswith("-")) :
        try:
            if param.startswith("+"):
                range_num = int(param[1:-1])
            else:
                range_num = int(param[:-1])
        except:
            await ctx.send("Invalid argument in \"Following\".")
            return False
        if range_num % 100 == 0:
            if param.startswith("+"):
                if range_num == 400:
                    return sortByRange(data, ["previousPitchDiff"], 400, 500)
                else:
                    return sortByRange(data, ["previousPitchDiff"], range_num, range_num + 99)
            else:
                return sortByRange(data, ["previousPitchDiff"], range_num - 99, range_num)
        else:
            await ctx.send("\"Following\" range must be divisible by 100.")
            return False
    return "Invalid"

async def byFollowingInningStart(ctx, data, param):
    if param.strip().lower() == "inning start" or param.strip().lower() == "inning starts":
        return sortByParameter(data, ["previousInningStart"], [True])
    else:
        return "Invalid"
            
async def byFollowingGameStart(ctx, data, param):
    if param.strip().lower() == "game start" or param.strip().lower() == "game starts":
        return sortByParameter(data, ["previousGameStart"], [True])
    else:
        return "Invalid"

def byGameStart(data):
    return sortByParameter(data, ["gameStart"], [True])

def byInningStart(data):
    return sortByParameter(data, ["inningStart"], [True])

async def bySessions(ctx, data, param):
    """
    Sorts data based on sessions can input range of sessions
    """
    if len(param.strip()) <= 6:
        await ctx.send("\"Session\" found no session(s) to sort by.")
        return False
    sessions = param[8:].strip().split("-")
    if (len(sessions) == 1):
        return sortByParameter(data, ["session"], [int(sessions[0])])
    else:
        return sortByRange(data, ["session"], int(sessions[0]), int(sessions[1]))

async def byFollowingCustomRange(ctx, data, param):
    if param.startswith("(") and param.endswith(")"):
        nums = param.split("-")
        try:
            first_range = int((nums[0][1:]))
            second_range = int((nums[1][:-1]))
        except:
            await ctx.send("Invalid argument in \"Following\".")
            return False
        return sortByRange(data, ["previousPitch"], first_range, second_range)
    else:
        return "Invalid"



