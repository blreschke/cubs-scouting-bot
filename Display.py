import discord
from Analyze import *
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

def displayPitches(data, showBreaks):
    """
    returns display of pitches from data
    """
    string_to_return = "```Pitch | Diff | Result\n------|------|-------\n"
    for PA in data:
        if (showBreaks and PA["gameStart"]):
            string_to_return += "------|------|-------\n "
        elif (showBreaks and PA["inningStart"]):
            string_to_return += "      |      |       \n "
        else:
            string_to_return += " "
        try:
            pitch = int(PA["pitch"])
            string_to_return += str(pitch)
            if (pitch < 10):
                string_to_return += "    | "
            elif (pitch < 100):
                string_to_return += "   | "
            elif (pitch == 1000):
                string_to_return += " | "
            else:
                string_to_return += "  | "
        except:
            string_to_return += "x    | "

        try:
            diff = int(PA["pitchDiff"])
            string_to_return += str(diff)
            if (diff < -99):
                string_to_return += " | "
            elif (diff < -9 or diff > 99):
                string_to_return += "  | "
            elif (diff < 0 or diff > 9):
                string_to_return += "   | "
            else:
                string_to_return += "    | "
        except:
            string_to_return += PA["pitchDiff"] + "    | "
        string_to_return += PA["oldResult"] + "\n"
    return string_to_return + "```"

def displaySwings(data, showBreaks):
    """
    returns display of swings from data
    """
    string_to_return = "```Swing | Pitch | Result\n------|-------|-------\n"
    for PA in data:
        if (showBreaks and PA["gameStart"]):
            string_to_return += "------|-------|-------\n "
        else:
            string_to_return += " "
        try:
            swing = int(PA["swing"])
            string_to_return += str(swing)
            if (swing < 10):
                string_to_return += "    |  "
            elif (swing < 100):
                string_to_return += "   |  "
            elif (swing == 1000):
                string_to_return += " |  "
            else:
                string_to_return += "  |  "
        except:
            string_to_return += "x    |  "

        try:
            pitch = int(PA["pitch"])
            string_to_return += str(pitch)
            if (pitch < 10):
                string_to_return += "    | "
            elif (pitch < 100):
                string_to_return += "   | "
            elif (pitch == 1000):
                string_to_return += " | "
            else:
                string_to_return += "  | "
        except:
            string_to_return += "x    | "
        string_to_return += PA["oldResult"] + "\n"
    return string_to_return + "```"

def displayRangePercentages(data, isExtended, isPitcher):
    """
    Returns display of Range Percentages from dataa
    """
    if (data == []):
        return "No data found for given parameters."
    data, sample_size = getRangePercentages(data, isExtended, isPitcher)
    sample_size = str(sample_size)
    string_to_return = "```Zone | Chance (" + sample_size + " PAs) \n-----|-------\n"
    if isExtended:
        string_to_return +=f"00s  | {data[0]}%\n"
        string_to_return +=f"50s  | {data[1]}%\n"
        string_to_return +=f"100s | {data[2]}%\n"
        string_to_return +=f"150s | {data[3]}%\n"
        string_to_return +=f"200s | {data[4]}%\n"
        string_to_return +=f"250s | {data[5]}%\n"
        string_to_return +=f"300s | {data[6]}%\n"
        string_to_return +=f"350s | {data[7]}%\n"
        string_to_return +=f"400s | {data[8]}%\n"
        string_to_return +=f"450s | {data[9]}%\n"
        string_to_return +=f"500s | {data[10]}%\n"
        string_to_return +=f"550s | {data[11]}%\n"
        string_to_return +=f"600s | {data[12]}%\n"
        string_to_return +=f"650s | {data[13]}%\n"
        string_to_return +=f"700s | {data[14]}%\n"
        string_to_return +=f"750s | {data[15]}%\n"
        string_to_return +=f"800s | {data[16]}%\n"
        string_to_return +=f"850s | {data[17]}%\n"
        string_to_return +=f"900s | {data[18]}%\n"
        string_to_return +=f"950s | {data[19]}%\n"
    else:
        string_to_return +=f"00s  | {data[0]}%\n"
        string_to_return +=f"100s | {data[1]}%\n"
        string_to_return +=f"200s | {data[2]}%\n"
        string_to_return +=f"300s | {data[3]}%\n"
        string_to_return +=f"400s | {data[4]}%\n"
        string_to_return +=f"500s | {data[5]}%\n"
        string_to_return +=f"600s | {data[6]}%\n"
        string_to_return +=f"700s | {data[7]}%\n"
        string_to_return +=f"800s | {data[8]}%\n"
        string_to_return +=f"900s | {data[9]}%\n"
    return string_to_return + "```"

def displayTensPercentages(data, isPitcher):
    """
    Returns display of Tens percentages from data
    """
    if (data == []):
        return "No data found for given parameters."
    data, sample_size = getTensPercentages(data, isPitcher)
    sample_size = str(sample_size)
    string_to_return = "```Zone | Chance (" + sample_size + " PAs) \n-----|-------\n"
    string_to_return +=f"x00s | {data[0]}%\n"
    string_to_return +=f"x10s | {data[1]}%\n"
    string_to_return +=f"x20s | {data[2]}%\n"
    string_to_return +=f"x30s | {data[3]}%\n"
    string_to_return +=f"x40s | {data[4]}%\n"
    string_to_return +=f"x50s | {data[5]}%\n"
    string_to_return +=f"x60s | {data[6]}%\n"
    string_to_return +=f"x70s | {data[7]}%\n"
    string_to_return +=f"x80s | {data[8]}%\n"
    string_to_return +=f"x90s | {data[9]}%\n"
    return string_to_return + "```"

def displayDiffPercentages(data, isPitcher):
    """
    Returns display of Diff percentages from data
    """
    if (data == []):
        return "No data found for given parameters."
    data, sample_size = getDiffPercentages(data, isPitcher)
    sample_size = str(sample_size)
    string_to_return = "```Zone  | Chance (" + sample_size + " PAs)\n------|-------\n"
    string_to_return +=f"-400s | {data[0]}%\n"
    string_to_return +=f"-300s | {data[1]}%\n"
    string_to_return +=f"-200s | {data[2]}%\n"
    string_to_return +=f"-100s | {data[3]}%\n"
    string_to_return +=f"-00s  | {data[4]}%\n"
    string_to_return +=f"+00s  | {data[5]}%\n"
    string_to_return +=f"+100s | {data[6]}%\n"
    string_to_return +=f"+200s | {data[7]}%\n"
    string_to_return +=f"+300s | {data[8]}%\n"
    string_to_return +=f"+400s | {data[9]}%\n"
    return string_to_return + "```"

def displaySwingDiffPercentages(data):
    """
    Returns display of Swing to next pitch diff percentages from data
    """
    if (data == []):
        return "No data found for given parameters."
    data, sample_size = getSwingDiffPercentages(data)
    sample_size = str(sample_size)
    string_to_return = "```Zone  | Chance (" + sample_size + " PAs)\n------|-------\n"
    string_to_return +=f"-400s | {data[0]}%\n"
    string_to_return +=f"-300s | {data[1]}%\n"
    string_to_return +=f"-200s | {data[2]}%\n"
    string_to_return +=f"-100s | {data[3]}%\n"
    string_to_return +=f"-00s  | {data[4]}%\n"
    string_to_return +=f"+00s  | {data[5]}%\n"
    string_to_return +=f"+100s | {data[6]}%\n"
    string_to_return +=f"+200s | {data[7]}%\n"
    string_to_return +=f"+300s | {data[8]}%\n"
    string_to_return +=f"+400s | {data[9]}%\n"
    return string_to_return + "```"

def displayHeatmap(data, isPitcher):
    """
    Returns pitch frequency heatmap
    """
    if (isPitcher):
        dict_path = "pitcherName"
        ab_name = "BF's"
    else:
        dict_path = "batterName"
        ab_name = "PA's"
    ticks = [0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5]
    labels = ['00s', '100s', '200s', '300s', '400s', '500s', '600s', '700s', '800s', '900s']
    name = data[0][-1][dict_path]
    dataarray = np.array([getRangeCount(data[0], False, isPitcher)])
    for dataset in data[1:]:
        dataarray = np.append(dataarray, [getRangeCount(dataset, False, isPitcher)], axis=0)
    dataarray = np.transpose(dataarray)
    size = 0
    for i in dataarray:
        for j in i:
            size += j
    plt.rcParams['xtick.bottom'] = plt.rcParams['xtick.labelbottom'] = False
    plt.rcParams['xtick.top'] = plt.rcParams['xtick.labeltop'] = True
    sns.heatmap(dataarray, annot=True, linewidths=.5, cbar=False)
    plt.title(name + " | " + str(size) + " " + ab_name)
    plt.xticks(ticks=ticks, labels=labels)
    plt.yticks(ticks=ticks, labels=labels, rotation=0)
    plt.savefig('heatmap.png')
    plt.clf()
    return 'heatmap.png'

def displayMatrix(pdata, p_isPitcher, bdata, b_isPitcher):
    """
    Returns pitch frequency heatmap
    """

    pdict_path = "pitcher"
    bdict_path = "batter"
    yticks = [0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5]
    xticks = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19]
    labels = ['00s', '100s', '200s', '300s', '400s', '500s', '600s', '700s', '800s', '900s']
    pname = pdata[0][-1][pdict_path]["name"]
    bname = bdata[-1][bdict_path]["name"]
    bcount, bsize = getRangePercentages(bdata, False, b_isPitcher)
#    bsize = 0
#    for i in bcount[0]:
#        bsize += i
    pcount, psize = getRangePercentages(pdata[0], False, p_isPitcher)
    dataarray = np.array([pcount])
    dataarray = np.append(dataarray, [bcount], axis=0)
    for dataset in pdata[1:]:
        pcount, psizeadd = getRangePercentages(dataset, False, p_isPitcher)
        psize += psizeadd
        dataarray = np.append(dataarray, [pcount], axis=0)
        dataarray = np.append(dataarray, [bcount], axis=0)
#    psize = 0
#    for i in dataarray:
#        for j in i:
#            psize += j
#    psize -= 10 * bsize
    dataarray = np.transpose(dataarray)
    plt.rcParams['xtick.bottom'] = plt.rcParams['xtick.labelbottom'] = False
    plt.rcParams['xtick.top'] = plt.rcParams['xtick.labeltop'] = True
    sns.heatmap(dataarray, annot=False, cbar=False)
    plt.vlines(x=[2, 4, 6, 8, 10, 12, 14, 16, 18], ymin=0, ymax=10, colors="white")
    plt.hlines(y=[1, 2, 3, 4, 5, 6, 7, 8, 9], xmin=0, xmax=20, colors="white")
    plt.title(pname + " | " + str(psize) + " BF || " + bname + " | " + str(bsize) + " PA")
    plt.xticks(ticks=xticks, labels=labels)
    plt.yticks(ticks=yticks, labels=labels, rotation=0)
    plt.savefig('matrix.png')
    plt.clf()
    return 'matrix.png'

async def displayList(ctx, data, display_count, display_breaks, display_function, client):
    # Show 50 max to stay in discord message limit
    shortened_data = mostRecent(data, display_count)

    to_display = display_function(shortened_data, display_breaks)

    # DISPLAYS, AND CONTROLLING WHEN TO ASK FOR NEXT
    if len(shortened_data) == len(data):
        await ctx.send(to_display)
        return
    else:
        await ctx.send(to_display + "Type \"Next\" For " + str(display_count) + " More Pitches")

    page = 0
    msg = await client.wait_for('message', check=lambda message: message.author == ctx.author)
    while msg.content.lower().strip() == "next":
        page += 1
        if ((page + 1) * display_count >= len(data)):
            shortened_data = data[:-(page * display_count)]
            to_display = display_function(shortened_data, display_breaks)
            await ctx.send(to_display)
            break
        else:
            shortened_data = mostRecent(data[:-(page * display_count)], display_count)
            to_display = display_function(shortened_data, display_breaks)
            await ctx.send(to_display + "Type \"Next\" For " + str(display_count) + " More Pitches")
            msg = await client.wait_for('message', check=lambda message: message.author == ctx.author)

async def displaySize(ctx, data):
    milr_count = 0
    mlr_count = 0

    for i in data:
        season = i["season"]
        if season < 0 and season > -100:
            milr_count += 1
        else:
            mlr_count += 1
    await ctx.send("```MLR  : " + str(mlr_count) + "\nMiLR : " + str(milr_count) + "```")

async def displayMostCommon(ctx, data, isPitcher):
    most_common, sample_size = getMostCommon(data, isPitcher)
    if (sample_size == 0):
        return "No data found for given parameters."
    if isPitcher:
        numbertype = "pitches"
    else:
        numbertype = "swings"
    string_to_send = "```most common " + numbertype + " out of " + str(sample_size) + " " + numbertype + ": \n"
    for pair in most_common:
        string_to_send += str(pair[0]) + ", used " + str(pair[1]) + " times\n"
    await ctx.send(string_to_send + "```")
