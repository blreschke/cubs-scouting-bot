import urllib.request
import json

def dataFromSource(source):
    """
    Return json data from a url
    
        Parameters:
            source - string url to website to load data from
        Return:
            json data loaded from site
            for redditball: a tuple containing dictionaries with at bat data
    """
    with urllib.request.urlopen(source) as url:
        data = json.loads(url.read())
    return data

def discordSnowflakesFromTeam(team):
    """
    Returns a list of discord snowflakes given a url
    
        Parameters:
            team - Team abbreviation of the team to pull snowflakes for
        Return:
            List containing strings of discord snowflakes for all players on the given team
    """
    data = dataFromSource("https://redditball.com/api/v1/players/byTeam/" + team)
    snowflakes = []
    for i in data:
        snowflakes.append(i["discordSnowflake"])
    return snowflakes

def verifyDiscordWithTeam(snowflake, team):
    """
    Checks if discord user is on a certain team

        Parameters:
            snowflake: current user's discord snowflake id
            team: team to check if user is on
        Return:
            boolean value of if the user is on the team
    """
    team_snowflakes = discordSnowflakesFromTeam(team)
    if str(snowflake) in team_snowflakes:
        return True
    else:
        return False

def playerByName(name, league):
    """
    access player data by name

        Parameter:
            name - name of player
        Return:
            data from https://redditball.com/api/v1/players/search?query=name
    """
    name = name.replace(" ", "%20")
    """
    if (league.upper() == "FCB"):
        return dataFromSource("https://fcb.redditball.com/api/v1/players/search?query=" + name)
    elif (league.upper() == "GIB"):
        return dataFromSource("https://gib.redditball.com/api/v1/players/search?query=" + name)
    else:
        return dataFromSource("https://redditball.com/api/v1/players/search?query=" + name)
    """
    return dataFromSource("https://www.swing420.com/api/players/name/" + name)

def activeTeamGame(team, is_milr):
    """
    Acess game data for current game for team

        Parameter:
            team - team abbreviation
        Return: 
            data from https://redditball.com/api/v1/games/active/team
    """
    if is_milr:
        source = "https://milr.redditball.com/api/v1/games/active/" + team
    else:
        source = "https://redditball.com/api/v1/games/active/" + team
    try:
        data = dataFromSource(source)
    except:
        data = False
    return data

def playerFromDiscord(snowflake, league):
    if (snowflake == None):
        return None
    if (league == "GIB"):
        source = "https://gib.redditball.com/api/v1/players/byDiscord?snowflake=" + str(snowflake)
    else:
        source = "https://redditball.com/api/v1/players/byDiscord?snowflake=" + str(snowflake)
    try:
        data = dataFromSource(source)
        to_return = data
    except:
        to_return = str(snowflake)
    return to_return