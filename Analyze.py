import math
from collections import Counter

def getInRange(numList, lowerBound, upperBound):
    """
    Returns list of integers in specified range
   
        Parameters:
            numList: List of integers to iterate
            lowerBound: INCLUSIVE lower bound of range
            upperBound: INCLUSIVE upper bound of range
        Return:
            List of numbers in numList within the specified range
    """
    in_range_list = []
    for i in numList:
        try:
            current_num = int(i)
            if lowerBound > upperBound:
                #i.e. 800 to 100
                if current_num >= lowerBound or current_num <= upperBound:
                    in_range_list.append(current_num)
            else:
                if current_num >= lowerBound and current_num <= upperBound:
                    in_range_list.append(current_num)
        except ValueError:
            #if there is non int value, ignore
            pass
    return in_range_list

def getDiff(num1, num2):
    """
    Calculate diff of two numbers within a range of -500 to 500
    
        Parameters:
            num1 - previous number
            num2 - current number
        Return:
            diff between numbers within range of -500 to 500
    """
    try:
        diff = int(num2) - int(num1)
        if diff <= -500 or diff > 500:
            if num2 > num1:
                return num2 - num1 - 1000
            else:
                return num2 - num1 + 1000
        else:
            return diff
    except ValueError:
        return "x"

def getPitchMatrix(numList):
    '''
    gets 2D array with matrix data
    '''
    #list where corresponding index to numList is the previous at bat
    matrix_data = getEmptyMatrix()
    previous_pitch = "x"
        
def mostRecent(dataList, recentNum):
    '''
    Returns list with only the most recent numbers (last recentNum)

        Parameters:
            numList: List of numbers we are iterating through
            recentNum: Size of the return list
        Return:
            List containing most recent numbers of size recentNum
    '''
    new_data_list = []
    for PA in reversed(dataList):
        if recentNum <= 0:
            break
        new_data_list.append(PA)
        recentNum -= 1
    new_data_list.reverse()
    return new_data_list


def getEmptyMatrix():
    '''
    returns an empty 2D tuple to store data in
    '''
    return ((0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
            (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
            (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
            (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
            (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
            (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
            (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
            (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
            (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
            (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0))  

def getRangePercentages(data, isExtended, isPitcher):
    percent_list = []
    sample_size = 0
    if (isPitcher):
        dict_path = "pitch"
    else:
        dict_path = "swing"
    if (isExtended):
        count_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        for i in data:
            try:
                index = int(i[dict_path] / 50) % 20
                count_list[index] += 1
                sample_size += 1
            except:
                pass
        for i in count_list:
            num = i / sample_size
            num = num * 100
            num = int(num)
            percent_list.append(num)
    else:
        count_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        for i in data:
            try:
                index = int(i[dict_path] / 100) % 10
                count_list[index] += 1
                sample_size += 1
            except:
                pass
        for i in count_list:
            num = i / sample_size
            num = num * 100
            num = int(num)
            percent_list.append(num)
    return percent_list, sample_size

def getRangeCount(data, isExtended, isPitcher):
    if (isPitcher):
        dict_path = "pitch"
    else:
        dict_path = "swing"
    if (isExtended):
        count_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        for i in data:
            try:
                index = int(i[dict_path] / 50) % 20
                count_list[index] += 1
            except:
                pass
    else:
        count_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        for i in data:
            try:
                index = int(i[dict_path] / 100) % 10
                count_list[index] += 1
            except:
                pass
    return count_list

def getTensPercentages(data, isPitcher):
    if (isPitcher):
        dict_path = "pitch"
    else:
        dict_path = "swing"
    percent_list = []
    sample_size = 0
    count_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for i in data:
        try:
            index = int(i[dict_path] / 10) % 10
            count_list[index] += 1
            sample_size += 1
        except:
            pass
    for i in count_list:
        num = i / sample_size
        num = num * 100
        num = int(num)
        percent_list.append(num)
    return percent_list, sample_size

def getDiffPercentages(data, isPitcher):
    percent_list = []
    sample_size = 0
    count_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for i in data:
        try:
            index = math.floor(i["pitchDiff"] / 100) + 5
            if index == 10:
                index = index - 1
            count_list[index] += 1
            sample_size += 1
        except:
            pass
    for i in count_list:
        num = i / sample_size
        num = num * 100
        num = int(num)
        percent_list.append(num)
    return percent_list, sample_size

def getMostCommon(data, isPitcher):
    if isPitcher:
        dict_path = "pitch"
    else:
        dict_path = "swing"
    number_list = []
    for i in data:
        try:
            number_list.append(int(i[dict_path]))
        except:
            pass
    most_common = Counter(number_list).most_common(3)
    return most_common, len(number_list)

def getSwingDiffPercentages(data):
    percent_list = []
    sample_size = 0
    count_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for i in data:
        try:
            index = math.floor(getDiff(i["previousSwing"], i["pitch"]) / 100) + 5
            count_list[index] += 1
            sample_size += 1
        except:
            pass
    for i in count_list:
        num = i / sample_size
        num = num * 100
        num = int(num)
        percent_list.append(num)
    return percent_list, sample_size
    
