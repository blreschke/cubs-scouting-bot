# Cubs Scouting Bot

The following project utilizes the [discord.py](https://discordpy.readthedocs.io/en/stable/) library to create a bot that outputs data analysis to help spot patterns in datasets taken from [redditball.com](https://gitlab.com/fakebaseball/website).

## Running the Bot

To run the bot, you will need to have discord.py installed on the current machine. In this directory, add a file titled `token.txt`, which must contain a valid discord bot token, which can be acquired through the [Discord developer portal](https://discord.com/developers/docs/intro). After adding the relevant bot to a discord server you occupy, running the `Bot.py` file will launch the bot.

## What is the Bot's Purpose?

The bot is used in relation to the online game [redditball](https://redditball.com/). The game mimics baseball, and centers around players inputting numbers in a 1-1000 range, with the offensive team trying to get as close to the defensive team's number as possible. This bot is used to provide analysis on opposing player's data, and data of the user to find patterns that may arise.
