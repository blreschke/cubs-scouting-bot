import discord
import os
from SiteAccess import *
from Display import *
from Analyze import *
from PlayerData import *
from discord.ext import commands
import Sorts
import Commands

client = commands.Bot(command_prefix = "!")
client.remove_command("help")

@client.command()
async def ping(ctx):
   await ctx.send("Pong!")

def is_cub():
   '''
   Command check if a player is a cub
   '''
   async def predicate(ctx):
      player = playerFromDiscord(ctx.author.id, "MLR")
      #print player who used the command in the console
      print("Command used by:" + player["name"])
      return verifyDiscordWithTeam(ctx.author.id, "CHC")
   return commands.check(predicate)


@client.command()
#@is_cub()
async def pitches(ctx):
   '''
   Print out pitch list

      Parameters:
         ctx - client get message and send messages
   '''
   command_length = 8
   isPitcher = True
   parameters, data, name_param = await Commands.initializeCommand(ctx, command_length, isPitcher)
   if (data == "EXIT"):
      await ctx.send("Player not found, please type their exact name")
      return
   elif (len(data) == 0):
      await ctx.send("No pitches found for \"" + name_param + "\".")
      return
   else:
      #Augment pitch list based on parameters
      filtered_data = await Commands.filter(ctx, data, parameters, ['LAST', 'SESSION', 'FOR', 'INNINGSTART', 'GAMESTART', 'INNING', 'OBC', 'OUTS', 'FOLLOWING', 'CURRENT', 'SEASON', 'LEAGUE'], isPitcher)
      await displayList(ctx, filtered_data['data'], filtered_data['display_count'], filtered_data['display_breaks'], displayPitches, client)

@client.command()
#@is_cub()
async def swings(ctx):
   '''
   Prints out swing list for a batter

      Parameters:
         ctx - client get message and send messages
   '''
   command_length = 7
   isPitcher = False
   parameters, data, name_param = await Commands.initializeCommand(ctx, command_length, isPitcher)
   if (data == "EXIT"):
      await ctx.send("Player not found, please type their exact name")
      return
   elif (len(data) == 0):
      await ctx.send("No swings found for \"" + name_param + "\".")
      return
   else:
      #Augment pitch list based on parameters
      filtered_data = await Commands.filter(ctx, data, parameters, ['LAST', 'SESSION', 'FOR', 'INNING', 'OBC', 'OUTS', 'FOLLOWING', 'CURRENT', 'SEASON', 'LEAGUE'], isPitcher)
      await displayList(ctx, filtered_data['data'], filtered_data['display_count'], filtered_data['display_breaks'], displaySwings, client)

@client.command()
#@is_cub()
async def pfreq(ctx):
   '''
   Print out pitch frequency

      Parameters:
         ctx - client get message and send messages
   '''
   command_length = 6
   isPitcher = True
   parameters, data, name_param = await Commands.initializeCommand(ctx, command_length, isPitcher)
   if (data == "EXIT"):
      await ctx.send("Player not found, please type their exact name")
      return
   elif (len(data) == 0):
      await ctx.send("No pitches found for \"" + name_param + "\".")
      return
   else:
      #Augment pitch list based on parameters
      filtered_data = await Commands.filter(ctx, data, parameters, ['EXTENDED', 'INNINGSTART', 'GAMESTART', 'SESSION', 'INNING', 'OBC', 'OUTS', 'FOLLOWING', 'CURRENT', 'SEASON', 'LEAGUE'], isPitcher)
      data = filtered_data['data']
      is_extended = filtered_data['is_extended']

      to_display = displayRangePercentages(data, is_extended, isPitcher)
      await ctx.send(to_display)

@client.command()
#@is_cub()
async def ptfreq(ctx):
   '''
   Print out tens frequency for a pitcher

      Parameters:
         ctx - client get message and send messages
   '''
   command_length = 7
   isPitcher = True
   parameters, data, name_param = await Commands.initializeCommand(ctx, command_length, isPitcher)
   if (data == "EXIT"):
      await ctx.send("Player not found, please type their exact name")
      return
   elif (len(data) == 0):
      await ctx.send("No pitches found for \"" + name_param + "\".")
      return
   else:
      #Augment pitch list based on parameters
      filtered_data = await Commands.filter(ctx, data, parameters, ['EXTENDED', 'FOR', 'INNINGSTART', 'GAMESTART', 'SESSION', 'INNING', 'OBC', 'OUTS', 'CURRENT', 'SEASON', 'LEAGUE'], isPitcher)
      data = filtered_data['data']

      to_display = displayTensPercentages(data, isPitcher)
      await ctx.send(to_display)

@client.command()
#@is_cub()
async def sfreq(ctx):
   '''
   Return list of pitches, with various parameters

      Parameters:
         ctx - client get message and send messages
      Send:
         Displayed list of data using Display.py's displayPitches
   '''
   command_length = 6
   isPitcher = False
   parameters, data, name_param = await Commands.initializeCommand(ctx, command_length, isPitcher)
   if (data == "EXIT"):
      await ctx.send("Player not found, please type their exact name")
      return
   elif (len(data) == 0):
      await ctx.send("No swings found for \"" + name_param + "\".")
      return
   else:
      #Augment pitch list based on parameters
      filtered_data = await Commands.filter(ctx, data, parameters, ['EXTENDED', "SESSION", 'INNING', 'OBC', 'OUTS', 'FOLLOWING', 'CURRENT', 'SEASON', 'LEAGUE'], isPitcher)
      data = filtered_data['data']
      is_extended = filtered_data['is_extended']

      to_display = displayRangePercentages(data, is_extended, isPitcher)
      await ctx.send(to_display)

@client.command()
#@is_cub()
async def stfreq(ctx):
   '''
   Print ten's digit swing frequencies

      Parameters:
         ctx - client get message and send messages
   '''
   command_length = 7
   isPitcher = False
   parameters, data, name_param = await Commands.initializeCommand(ctx, command_length, isPitcher)
   if (data == "EXIT"):
      await ctx.send("Player not found, please type their exact name")
      return
   elif (len(data) == 0):
      await ctx.send("No pitches found for \"" + name_param + "\".")
      return
   else:
      #Augment pitch list based on parameters
      filtered_data = await Commands.filter(ctx, data, parameters, ['EXTENDED', 'FOR', 'SESSION', 'INNING', 'OBC', 'OUTS', 'CURRENT', 'SEASON', 'LEAGUE'], isPitcher)
      data = filtered_data['data']

      to_display = displayTensPercentages(data, isPitcher)
      await ctx.send(to_display)

@client.command()
#@is_cub()
async def pdfreq(ctx):
   '''
   Print pitch diff frequencies

      Parameters:
         ctx - client get message and send messages
   '''
   command_length = 7
   isPitcher = True
   parameters, data, name_param = await Commands.initializeCommand(ctx, command_length, isPitcher)
   if (data == "EXIT"):
      await ctx.send("Player not found, please type their exact name")
      return
   elif (len(data) == 0):
      await ctx.send("No pitches found for \"" + name_param + "\".")
      return
   else:
      #Augment pitch list based on parameters
      filtered_data = await Commands.filter(ctx, data, parameters, ['INNINGSTART', 'GAMESTART', 'SESSION', 'INNING', 'OBC', 'OUTS', 'FOLLOWING', 'CURRENT', 'SEASON', 'LEAGUE'], isPitcher)
      data = filtered_data['data']

      to_display = displayDiffPercentages(data, isPitcher)
      await ctx.send(to_display)

@client.command()
#@is_cub()
async def psdfreq(ctx):
   '''
   Print pitch diff frequencies

      Parameters:
         ctx - client get message and send messages
   '''
   command_length = 8
   isPitcher = True
   parameters, data, name_param = await Commands.initializeCommand(ctx, command_length, isPitcher)
   if (data == "EXIT"):
      await ctx.send("Player not found, please type their exact name")
      return
   elif (len(data) == 0):
      await ctx.send("No pitches found for \"" + name_param + "\".")
      return
   else:
      #Augment pitch list based on parameters
      filtered_data = await Commands.filter(ctx, data, parameters, ['INNINGSTART', 'GAMESTART', 'SESSION', 'INNING', 'OBC', 'OUTS', 'FOLLOWING', 'CURRENT', 'SEASON', 'LEAGUE'], isPitcher)
      data = filtered_data['data']

      to_display = displaySwingDiffPercentages(data)
      await ctx.send(to_display)

@client.command()
#@is_cub()
async def ssize(ctx):
   '''
   prints out sample size for a player's swings
   '''
   command_length = 6
   isPitcher = False
   parameters, data, name_param = await Commands.initializeCommand(ctx, command_length, isPitcher)
   filtered_data = await Commands.filter(ctx, data, parameters, ['INNINGSTART', 'GAMESTART', 'SESSION', 'INNING', 'OBC', 'OUTS', 'FOLLOWING', 'CURRENT', 'SEASON', 'LEAGUE'], isPitcher)
   await displaySize(ctx, filtered_data['data'])

@client.command()
#@is_cub()
async def psize(ctx):
   '''
   prints out sample size for a player's pitches
   '''
   command_length = 6
   isPitcher = True
   parameters, data, name_param = await Commands.initializeCommand(ctx, command_length, isPitcher)
   filtered_data = await Commands.filter(ctx, data, parameters, ['INNINGSTART', 'GAMESTART', 'SESSION', 'INNING', 'OBC', 'OUTS', 'FOLLOWING', 'CURRENT', 'SEASON', 'LEAGUE'], isPitcher)
   await displaySize(ctx, filtered_data['data'])

@client.command()
#@is_cub()
async def ptop(ctx):
   '''
   prints out a players 3 most common pitches
   '''
   command_length = 5
   isPitcher = True
   parameters, data, name_param = await Commands.initializeCommand(ctx, command_length, isPitcher)
   filtered_data = await Commands.filter(ctx, data, parameters, ['INNINGSTART', 'GAMESTART', 'SESSION', 'INNING', 'OBC', 'OUTS', 'FOLLOWING', 'CURRENT', 'SEASON', 'LEAGUE', 'FOR'], isPitcher)
   await displayMostCommon(ctx, filtered_data['data'], isPitcher)

@client.command()
#@is_cub()
async def stop(ctx):
   '''
   prints out a players 3 most common swings
   '''
   command_length = 5
   isPitcher = False
   parameters, data, name_param = await Commands.initializeCommand(ctx, command_length, isPitcher)
   filtered_data = await Commands.filter(ctx, data, parameters, ['SESSION', 'INNING', 'OBC', 'OUTS', 'FOLLOWING', 'CURRENT', 'SEASON', 'LEAGUE', 'FOR'], isPitcher)
   await displayMostCommon(ctx, filtered_data['data'], isPitcher)

@client.command()
#@is_cub()
async def heatmap(ctx):
   '''
   Print out pitch frequency heatmap

      Parameters:
         ctx - client get message and send messages
   '''
   command_length = 8
   isPitcher = True
   parameters, data, name_param = await Commands.initializeCommand(ctx, command_length, isPitcher)
   if (data == "EXIT"):
      await ctx.send("Player not found, please type their exact name")
      return
   elif (len(data) == 0):
      await ctx.send("No pitches found for \"" + name_param + "\".")
      return
   else:
      #Augment pitch list based on parameters
      filtered_data = await Commands.filter(ctx, data, parameters, ['INNINGSTART', 'GAMESTART', 'SESSION', 'INNING', 'OBC', 'OUTS', 'FOLLOWING', 'CURRENT', 'SEASON', 'LEAGUE'], isPitcher)
      data = filtered_data['data']

      sorteddata = []
      databuckets = ['00s', '100s', '200s', '300s', '400s', '500s', '600s', '700s', '800s', '900s']
      for bucket in databuckets:
         sorteddata.append(await Sorts.byFollowingRange(ctx, data, bucket))
      to_display = displayHeatmap(sorteddata, isPitcher)
      await ctx.send(file=discord.File(to_display))
      os.remove(to_display)

@client.command()
#@is_cub()
async def matrix(ctx):
   '''
   Print out pitch frequency matrix

      Parameters:
         ctx - client get message and send messages
   '''
   command_length = 8
   p_isPitcher = True
   b_isPitcher = False
   parameters, pdata, pname = await Commands.initializeCommand(ctx, command_length, p_isPitcher)
   bname = parameters[0].strip()
   parameters = parameters[1:]
   bdata = await getPlayerData(ctx, bname, b_isPitcher)
   if (bdata == "EXIT"):
      await ctx.send("Player not found, please type their exact name")
      return
   if (len(pdata) == 0):
      await ctx.send("No pitches found for \"" + pname + "\".")
      return
   else:
      if (len(bdata) ==0):
         await ctx.send("No swings found for \"" + bname + "\".")
         return
      else:
         #Augment pitch list based on parameters
         filtered_pdata = await Commands.filter(ctx, pdata, parameters, ['INNINGSTART', 'GAMESTART', 'SESSION', 'INNING', 'OBC', 'OUTS', 'FOLLOWING', 'CURRENT', 'SEASON', 'LEAGUE'], p_isPitcher)
         pdata = filtered_pdata['data']
         filtered_bdata = await Commands.filter(ctx, bdata, parameters, ['INNINGSTART', 'GAMESTART', 'SESSION', 'INNING', 'OBC', 'OUTS', 'FOLLOWING', 'CURRENT', 'SEASON', 'LEAGUE'], b_isPitcher)
         bdata = filtered_bdata['data']

         sortedpdata = []
         databuckets = ['00s', '100s', '200s', '300s', '400s', '500s', '600s', '700s', '800s', '900s']
         for bucket in databuckets:
            sortedpdata.append(await Sorts.byFollowingRange(ctx, pdata, bucket))
         to_display = displayMatrix(sortedpdata, p_isPitcher, bdata, b_isPitcher)
         await ctx.send(file=discord.File(to_display))
         os.remove(to_display)

@client.command()
#@is_cub()
async def help(ctx):
   command_length = 5
   if len(ctx.message.content) <= command_length:
      await ctx.send("""**Command List**
>>> `pitches`: Shows pitch list from a player
`swings`: Shows swing list from a player
`pfreq`: Shows pitch range frequencies
`tfreq`: Shows frequency of the tens digit
`top`: Shows top 3 most frequent pitches or swings
`size`: Shows their sample size between leagues
Type \"!help <command name>\" for available parameters for each function""")

   else:
      #split string after command into list
      parameter = ctx.message.content[(command_length):].strip().lower()
      if (parameter == "pitches"):
         await ctx.send("""**Pitches** : Shows pitch list from a player\n
__Parameters__
>>> `last`: Followed by a number to only display the X most recent.
`inning start`: Only gets inning starts.
`game start`: Only gets game starts.
`current`: Followed by MiLR or MLR for only pitches from current game there.
`inning`: Followed by inning(s) you want to show. 
`obc`: followed by OBC code(s) you want to show.
`outs`: followed by out(s) you want to include.
`seasons`: followed by seasons you want to include. Follow number with "i" to search milr.
`league`: followed by "MLR" or "MiLR" to search a specific league.
`following`: followed by ranges, results, specific numbers, diffs. Type \"!help following\" for examples""")
      elif (parameter == "swings"):
         await ctx.send("""**Swings** : Shows swing list from a player\n
__Parameters__
>>> `last`: Followed by a number to only display the X most recent.
`inning start`: Only gets inning starts.
`game start`: Only gets game starts.
`current`: Followed by MiLR or MLR for only pitches from current game there.
`inning`: Followed by inning(s) you want to show. 
`obc`: followed by OBC code(s) you want to show.
`outs`: followed by out(s) you want to include.
`seasons`: followed by seasons you want to include. Follow number with "i" to search milr.
`league`: followed by "MLR" or "MiLR" to search a specific league.
`following`: followed by ranges, results, specific numbers, diffs. Type \"!help following\" for examples""")
      elif (parameter == "pfreq"):
         await ctx.send("""**pfreq** : Shows pitch range frequencies\n
__Parameters__
>>> `extended`: Ranges are by 50 instead of 100
`inning start`: Only gets inning starts.
`game start`: Only gets game starts.
`current`: Followed by MiLR or MLR for only pitches from current game there.
`inning`: Followed by inning(s) you want to show. 
`obc`: followed by OBC code(s) you want to show.
`outs`: followed by out(s) you want to include.
`seasons`: followed by seasons you want to include. Follow number with "i" to search milr.
`league`: followed by "MLR" or "MiLR" to search a specific league.
`for`: followed by a range to show.""")
      elif (parameter == "tfreq"):
         await ctx.send("""**tfreq** : Shows frequency of the tens digit.\n
__Parameters__
>>>`inning start`: Only gets inning starts.
`game start`: Only gets game starts.
`current`: Followed by MiLR or MLR for only pitches from current game there.
`inning`: Followed by inning(s) you want to show. 
`obc`: followed by OBC code(s) you want to show.
`outs`: followed by out(s) you want to include.
`seasons`: followed by seasons you want to include. Follow number with "i" to search milr.
`league`: followed by "MLR" or "MiLR" to search a specific league.
`for`: followed by a range to show.""")
      elif (parameter == "following"):
         await ctx.send("""**following** : Parameter for \"PITCHES\", \"SWINGS\" 
__Arguments__
>>> `results`: Shows PA following specific results. Also takes \"STEALS\", \"OUTS\", and \"HITS\"
`ranges`: Shows PA following specific pitch range, i.e. \"following 600s\"
`diffs`: Shows PA following specific diff range, i.e. \"following +00s\", \"following -300s\"
`pitch`: Shows PA following specific pitch, i.e. \"following 69\"""")

client.run(open("token.txt").readlines()[0])
