import SiteAccess
from Analyze import getDiff

def playerData(mlr_player_num, is_pitches):

    if (is_pitches):
        data = SiteAccess.dataFromSource("https://www.swing420.com/api/plateappearances/pitching/mlr/" + str(mlr_player_num))

        milr_data = SiteAccess.dataFromSource("https://www.swing420.com/api/plateappearances/pitching/milr/" + str(mlr_player_num))


    else:
        data = SiteAccess.dataFromSource("https://www.swing420.com/api/plateappearances/batting/mlr/" + str(mlr_player_num))

        milr_data = SiteAccess.dataFromSource("https://www.swing420.com/api/plateappearances/batting/milr/" + str(mlr_player_num))
    for i in milr_data:
        i["season"] = i["season"] - 100
    all_data = milr_data + data
    sorted_data = sorted(all_data, key = lambda i: i["season"])
    return addExtraData(sorted_data)

async def getPlayerData(ctx, name, isPitcher):
    print(ctx.author)
    try:
        player_id = int(name)
    except:
        data = SiteAccess.playerByName(name, "MLR")
        if data["playerID"] == 0:
            return "EXIT"
        else:
            player_id = data["playerID"]
    return playerData(player_id, isPitcher)


def sortByParameter(data, dictionaryPath, toSortBy):
    """
    Sorts PA list, data, by given parameter

        Parameters:
            data - PA dictionary list
            dictionaryPath - path list to access where parameter is stored in the dictionary
            toSortBy - parameter list of parameters to check for at desired place
        Return:
            Sorted list only including data fitting the parameter
    """
    sorted_list = []
    for i in data:
        curr_data_point = i
        for j in dictionaryPath:
            curr_data_point = curr_data_point[str(j)]
        if curr_data_point in toSortBy:
            sorted_list.append(i)
    return sorted_list

def sortByRange(data, dictionaryPath, lowerBound, upperBound):

    sorted_list = []
    if (lowerBound == 0 and dictionaryPath != ["previousPitchDiff"]):
        lowerBound = 1000
    for i in data:
        curr_data_point = i
        for j in dictionaryPath:
            curr_data_point = curr_data_point[str(j)]
        try:
            if lowerBound > upperBound:
                if curr_data_point >= lowerBound or curr_data_point <= upperBound:
                    sorted_list.append(i)
            else:
                if curr_data_point >= lowerBound and curr_data_point <= upperBound:
                    sorted_list.append(i)
        except:
            pass
    return sorted_list

def addExtraData(data):
    '''
    Adds extra data to the dictionary, probably not a clean way to do it
    '''
    prev_inning = "None"
    prev_game = "None"
    prev_pitch = "x"
    prev_result = "x"
    prev_diff = "x"
    prev_swing = "x"
    prev_is = False
    prev_gs = False
    for PA in data:
        if PA["pitch"] == 0:
            PA["pitch"] = "x"
            PA["swing"] = "x"
        set_inning = False
        set_game = False
        if PA["inning"] != prev_inning:
            set_inning = True
        if PA["gameID"] != prev_game:
            set_game = True

        set_pitch_diff = "x'"
        try:
            set_pitch_diff = getDiff(prev_pitch, PA["pitch"])
        except:
            set_pitch_diff = "x"

        #PA["beforeState"]["OBC"] = calcOBC(PA["beforeState"]["firstOccupied"], PA["beforeState"]["secondOccupied"], PA["beforeState"]["thirdOccupied"])
        PA["inningStart"] = set_inning
        PA["gameStart"] = set_game
        PA["pitchDiff"] = set_pitch_diff
        PA["previousResult"] = prev_result.lower()
        PA["previousPitch"] = prev_pitch
        PA["previousPitchDiff"] = prev_diff
        PA["previousSwing"] = prev_swing
        PA["previousInningStart"] = prev_is
        PA["previousGameStart"] = prev_gs
        PA["league"] = PA["league"].lower();


        prev_result = PA["oldResult"]
        prev_inning = PA["inning"]
        prev_game = PA["gameID"]
        prev_pitch = PA["pitch"]
        prev_swing = PA["swing"]
        prev_gs = set_game
        prev_is = set_inning
        prev_diff = set_pitch_diff
    return data

def calcOBC(first, second, third):
    '''
    calculated OBC code
    '''
    if not first and not second and not third:
        return "0"
    elif first and not second and not third:
        return "1"
    elif not first and second and not third:
        return "2"
    elif not first and not second and third:
        return "3"
    elif first and second and not third:
        return "4"
    elif first and not second and third:
        return "5"
    elif not first and second and third:
        return "6"
    else:
        return "7"

