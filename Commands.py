import Sorts
from PlayerData import *
from SiteAccess import *
from Analyze import *
from Display import *

async def initializeCommand(ctx, command_length, isPitcher):
    if len(ctx.message.content) <= command_length:
        await ctx.send("No player found")
        return
    else:
        # split string after command into list
        parameters = ctx.message.content[(command_length):]
        parameters = parameters.split(";")

        # first parameter should be name of player, access id through playerByName()
        name_param = parameters[0].strip()
        data = await getPlayerData(ctx, name_param, isPitcher)
        return parameters[1:], data, name_param

async def filter(ctx, data, parameters, filters, isPitcher):
    display_breaks = True   # display breaks for commands that use it, unless the data filtering makes breaks useless
    display_count = 25  # how many to display
    is_extended = False     # display extended frequency data
    for param in parameters:
        param = param.strip()
        param = param.lower()

        # LAST - Only includes most recent pitches, up to a parameter int
        if 'LAST' in filters and param.startswith("last"):
            try:
                last_int = int(param[4:].strip())
            except:
                await ctx.send("Invalid number after \"LAST\" parameter")
                return
            if last_int < 25:
                display_count = last_int

        # FOR - for specified range
        elif 'FOR' in filters and param.startswith("for"):
            data = await Sorts.byPitchRange(ctx, data, param, isPitcher)
            display_breaks = False

        # INNINGSTART - Only includes inning starts
        elif 'INNINGSTART' in filters and param in ["inningstart", "inningstarts", "inning starts", "inning start"]:
            data = Sorts.byInningStart(data)
            display_breaks = False

        # GAMESTART - Only include game starts
        elif 'GAMESTART' in filters and param in ["gamestart", "gamestarts", "game starts", "game start"]:
            data = Sorts.byGameStart(data)
            display_breaks = False

        # INNING - Only includes specified inning(s)
        elif 'INNING' in filters and param.startswith("inning"):
            data = await Sorts.byInnings(ctx, data, param)

        elif 'SESSION' in filters and param.startswith("session"):
            data = await Sorts.bySessions(ctx, data, param)

        # OBC - Only includes specific obcs
        elif 'OBC' in filters and param.startswith("obc"):
            data = await Sorts.byOBC(ctx, data, param)
            display_breaks = False

        # OUTS - Only include with specific out
        elif 'OUTS' in filters and param.startswith("out"):
            data = await Sorts.byOuts(ctx, data, param)

        # following - Includes pitches follow specific result
        elif 'FOLLOWING' in filters and param.startswith("following"):
            data = await Sorts.byFollowing(ctx, data, param)
            display_breaks = False

        # CURRENT - Only include current game from a league
        elif 'CURRENT' in filters and param.startswith("current"):
            data = await Sorts.byCurrent(ctx, data, param)

        # SEASON - Includes specfic seasons
        elif 'SEASON' in filters and param.startswith("season"):
            data = await Sorts.bySeason(ctx, data, param)

        # LEAGUE - Includes specific leagues
        elif 'LEAGUE' in filters and param.startswith("league"):
            data = await Sorts.byLeague(ctx, data, param)

        # extended - Extended frequency data
        elif 'EXTENDED' in filters and param.startswith("extend"):
            is_extended = True

        # if parameter is none of the above
        else:
            await ctx.send("\"" + param + "\" not recognized or supported for this query")
            return

        # data == false if a parameter printed an error message, so end
        if data == False:
            return
    return {'data': data, 'display_breaks': display_breaks, 'display_count': display_count, 'is_extended': is_extended}

